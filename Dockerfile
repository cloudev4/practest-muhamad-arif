FROM alpine:3.17.0

RUN apk add --no-cache nginx

COPY practest.html /usr/share/nginx/html/practest.html

RUN rm -rf /etc/nginx/http.d/default.conf

COPY default.conf /etc/nginx/http.d/default.conf

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]