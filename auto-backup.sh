#!/bin/bash

USER="root"
PASSWORD="practest1234"
DATABASE="db_practest"
PORT="3307"
HOST="localhost"
OUTPUT="/mnt/d/Project/Loker/DevOps/db_backup"

usage() {
    echo "       $0 backup Database"
    echo "-h    display this help and exit"
    echo "-l    list all existing backups and exit"
}

while getopts hl opt
do
    case $opt in
        h) usage; exit 0;;
        l) ls -lh $OUTPUT; exit 0;;
    esac
done

# Backup database
mysqldump --user=$USER --password=$PASSWORD --port=$PORT --host=$HOST  $DATABASE > $OUTPUT/db_backup_$(date +%Y%m%d%H%M%S).sql
